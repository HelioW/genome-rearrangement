-- compile using ghc ––make reversal_distance.hs

import System.Environment
import Data.List

main = do
       [str_n, str_x] <- getArgs
       let n = read str_n :: Int
           x = map (\x -> read x :: Int) $ words str_x
       putStrLn $ show $ reversal_distance (n, x)


type Genome = (Int, [Int])

reversal_distance :: Genome -> Int
reversal_distance (n, x) =  fst $ head $ filter f $ iterate (reversal_candidates upper_bound) (0, [(n, x)])
    where upper_bound = count_bp (n, x)
          f (d, candidates) = any is_sorted candidates
          is_sorted (n, x) = x == sorted
          sorted = sort x 
                

reversal_candidates :: Int -> (Int, [Genome]) -> (Int, [Genome])
reversal_candidates bound (d, gs) = (d+1, concat $ map (reversal_candidates' (bound-d-1)) gs)

reversal_candidates' :: Int -> Genome -> [Genome]
reversal_candidates' bound (n, x) = filter less_bp $ map (apply_reversal (n, x)) [(i, j) | i <- [0..(n-1)], j <- [(i+2)..n]]
    where less_bp g = count_bp g <= 2 * bound


apply_reversal :: Genome -> (Int, Int) -> Genome
apply_reversal (n, x) (i, j) = (n, concat [before, reversed, after])
    where before = take i x
          reversed = reverse $ drop i $ take j x
          after = drop j x


count_bp :: Genome -> Int
count_bp (n, x) = length $ filter (is_breakpoint (n, x)) [0..n]


is_breakpoint :: Genome -> Int -> Bool
is_breakpoint (n, x) 0 = x !! 0 /= 1
is_breakpoint (n, x) i = if i /= n then (a - b /= 1 && a - b /= -1)
                         else x !! (n-1) /= n
                         where a = x !! i
                               b = x !! (i-1)



