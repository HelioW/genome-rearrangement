"""
compute the reversal distance between two unsigned permutations (d <= 8)
using upper and lower bounds based on breakpoints during iterative BFS search
"""


_upper_bound = float('Inf')


def inverse_permutation(perm):
    """return the inverse of a permutation"""
    p = [None] * len(perm)
    for i, n in enumerate(perm):
        p[n - 1] = i + 1
    return p


def apply_permutation(p, a):
    """return the composite of two permutations (function composition)"""
    t = []
    for n in a:
        t.append(p[n - 1])
    return t


def count_breakpoints(a):
    """return the number of breakpoints in a permutation"""
    n = len(a)
    count = 0
    for i in xrange(n-1):
        u = a[i+1] - a[i]
        if u != 1 and u != -1:
            count += 1
    if a[0] != 1:
        count += 1
    if a[-1] != n:
        count += 1
    return count


def apply_reversal(p, i, j):
    """apply a reversal to the permutation p between i and j-1"""
    p[i:j] = reversed(p[i:j])


def reversal_candidates(bp, p, bound, depth):
    """
    return all permutatations after one reversal from p for which
    the reversal distance is less than bound (using rd >= bp/2)
    sorted by increasing number of breakpoints
    """
    global _upper_bound
    candidates = []
    for i in xrange(len(p)-1):
        for j in xrange(i+2, len(p)+1):
            copy_p = p[:]
            apply_reversal(copy_p, i, j)
            count = count_breakpoints(copy_p)
            if count == 0:
                return True, []
            if count > bp:  # using the theorem
                continue
            if count > 2 * bound:  # using the lower bound of sub-tree height
                continue
            _upper_bound = min(_upper_bound, depth + count)  # update the upper bound
            candidates.append((count, copy_p))
    candidates.sort()
    return False, candidates


def reversal_distance(perm, b=None):
    """
    return the reversal distance between two unsigned permutatations
    using an exact branch-and-bound algorithm
    """
    global _upper_bound
    n = len(perm)
    sorted_perm = range(1, n+1)
    if b is not None:  # when the reference permutation is not identity
        inv_b = inverse_permutation(b)
        perm = apply_permutation(inv_b, perm)
    _upper_bound = count_breakpoints(perm)
    if _upper_bound == 0:  # when perm is already sorted
        return 0
    current_distance = 1
    current_perms = [(_upper_bound, perm)]
    while current_distance < _upper_bound:
        bound = _upper_bound - current_distance  # upper bound for sub-tree height
        next_perms = []
        for bp_perm, next_perm in current_perms:
            can_be_sorted, next_candidates = reversal_candidates(bp_perm, next_perm, bound, current_distance)
            if can_be_sorted:
                return current_distance
            next_perms.extend(next_candidates)
        current_perms = next_perms
        current_distance += 1
    return _upper_bound


def test():
    import time
    p = [5,6,3,4,1,2]
    print reversal_distance(p)
    # p, q = [1,2,4,6,3,7,8,5], [2,4,6,8,7,5,3,1]
    # t0 = time.time()
    # print reversal_distance(p, q),
    # print 'time elapsed', time.time() - t0
    
    # test on the given example
    a = 'LHFEBADCKIJGM'
    b = 'ABCDEFGHIJKLM'
    a = map(lambda x: b.index(x)+1, a)
    t0 = time.time()
    print reversal_distance(a)
    print time.time() - t0
    

if __name__ == '__main__':
    test()
    


