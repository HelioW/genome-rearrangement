def adjacency_list(nodes, edge_list):
    """construct the adjacency list from edge list of an undirected graph"""
    adj = {}
    for i in nodes:
        adj[i] = set()
    for u, v in edge_list:
        adj[u].add(v)
        adj[v].add(u)
    return adj


def count_connected_component(nodes, adj_list):
    """returns the number of CC in undirected graph"""
    count = 0
    visited = {}
    for node in nodes:
        visited[node] = False
    
    def visit(node):
        visited[node] = True
        for neighb in adj_list[node]:
            if not visited[neighb]:
                visit(neighb)
    
    for node in nodes:
        if not visited[node]:
            visit(node)
            count += 1
    return count


def breakpoint_graph_nodes(perm):
    """nodes in breakpoint graph for signed permutation"""
    nodes = [0] * (2 * len(perm))
    for j in range(len(perm)):
        i = perm[j]
        if i > 0:
            nodes[2*j+1] = 2*i
            nodes[2*j] = 2*i - 1
        else:
            nodes[2*j+1] = -2*i - 1
            nodes[2*j] = -2*i
    return nodes


def breakpoint_graph(perm):
    """breakpoint graph for signed permutation"""
    nodes = breakpoint_graph_nodes(perm)
    n = len(perm)
    black_edges = [(nodes[2*j+1], nodes[2*j+2]) for j in xrange(n-1)]
    black_edges.extend([(0, nodes[0]), (2*n+1, nodes[-1])])  # telomeres
    grey_edges = [(2*i, 2*i+1) for i in xrange(n+1)]
    nodes.extend([0, 2*n+1])
    return nodes, adjacency_list(nodes, black_edges + grey_edges)


def dcj_distance(perm):
    """DCJ distance for signed permutation"""
    nodes, adj_list = breakpoint_graph(perm)
    n = (max(nodes)-1) / 2
    return n + 1 - count_connected_component(nodes, adj_list)


def count_breakpoints(perm):
    """count the number of breakpoints in a breakpoint graph for signed genome"""
    nodes = breakpoint_graph_nodes(perm)
    n = len(perm)
    black_edges = [(nodes[2*j+1], nodes[2*j+2]) for j in xrange(n-1)]
    black_edges.extend([(0, nodes[0]), (2*n+1, nodes[-1])])  # telomeres
    count = 0
    for i in xrange(n+1):
        if (2*i, 2*i+1) not in black_edges:
           count += 1
    return count


if __name__ == '__main__':
    import urllib2
    sock = urllib2.urlopen('http://lbbe.univ-lyon1.fr/projets/tannier/INSA/MIG/droso_orthologies_filtered.txt') 
    data = sock.read()                            
    sock.close()  

    genome = []
    for line in data.split('\n')[1:-1]:
        line = line.rstrip().split()
        gene, sign = line[0], line[-1]
        genome.append(int(gene) * int(sign))
    
    distance = dcj_distance(genome)
    print 'exact DCJ distance', distance
    
    
    from math import log
    n = float(len(genome))
    c = count_breakpoints(genome)
    print 'estimated distance', log(1 - c*(2*n+1)/(2*n*(n+1))) / log(1 - 1/(n*(n+1)) - 2/(n+1))
