Projet de génomique
===================
**Mesures de distances génomiques**

*Hélio Wang & Solène Granjeon*.

----------


Distance d'inversion
-------------

> **Note:**

> - Le calcul de la distance d'inversion étant un problème NP-difficile, pour des instances avec `n > 9`, essayez les programmes avec précaution.
> - Pour les implémentations BFS, il n'y a **pas de limite supérieure autorisée pour l'espace mémoire** intégrée aux programmes. Pensez à surveiller la mémoire et faire`Ctrl+C` au moment convenu.
> - Nous avons donné pour chaque implémentation un exemple d'utilisation dans la console Python2, c'est-à-dire dans Interactive Shell avec la commande `python2`. Il est évidemment possible de modifier les scripts, puis les exécuter directement, par exemple avec la commande ```python2 reversal_distance.py```.


### Recherche exhaustive BFS

Le fichier correspondant est `reversal_distance_exhaustive.py`.

Un exemple d'utilisation dans la console Python2 :
```
from reversal_distance_exhaustive import reversal_distance_exhaustive as rd1
p = [5, 6, 3, 4, 1, 2]
print rd1(p) 
```


### Branch-and-bound BFS


Le fichier correspondant est `reversal_distance_bfs.py`. Pour la version avec mémoïsation (plus rapide pour petites permutations), utilisez `reversal_distance_bfs_memo.py`.

Un exemple d'utilisation dans la console Python2 :
```
from reversal_distance_bfs import reversal_distance as rd2
p = [1, 5, 6, 8, 7, 4, 2, 3]
print rd2(p) 
```
Pour cette implémentation, il est aussi possible de calculer la distance **entre deux génomes** :
```
p, q = [1,2,4,6,3,7,8,5], [2,4,6,8,7,5,3,1]
print rd2(p, q)
```



### Branch-and-bound DFS

Le fichier correspondant est `reversal_distance_dfs.py`.

Un exemple d'utilisation dans la console Python2 :
```
from reversal_distance_dfs import reversal_distance as rd3
p = [1, 5, 6, 8, 7, 4, 2, 3]
print rd3(p) 
```
Pour cette implémentation, il est aussi possible de calculer la distance **entre deux génomes** :
```
p, q = [1,2,4,6,3,7,8,5], [2,4,6,8,7,5,3,1]
print rd3(p, q)
```


### Branch-and-bound BFS en Haskell (just for fun)

Le fichier correspondant est `reversal_distance.hs`. Pour utiliser le programme pré-compilé, fournissez comme premier argument la taille de la permutation et en second argument la permutation séparée par des espaces, par exemple :
```
./reversal_distance 8 "1 5 6 8 7 4 2 3"
```
Il est possible de re-compiler le code avec ` ghc ––make reversal_distance.hs`.

Afin de tester le programme directement dans la console Python2 comme les implémentations précédentes, nous avons aussi fait un wrapper du binaire pré-compilé dans le fichier `haskell_wrapper.py`.
Voici un exemple d'utilisation dans la console Python2 :
```
from haskell_wrapper import reversal_distance_haskell as rd4
p = [1, 5, 6, 8, 7, 4, 2, 3]
print rd4(p) 
```

### Calcul des distances d'inversion pour toutes les permutations de taille 6, 7 ou 8

Le fichier correspondant est `all_reversal_distances.py`.  Pour l'utiliser, dé-commenter une ou plusieurs des trois dernières lignes du script :
```
# all_six_reversal_distances()
# all_seven_reversal_distances()
# all_eight_reversal_distances()
```
puis utlisez la commande 
```
python2 all_reversal_distances.py
```

Si l'interpréteur `pypy` est installé, il est conseillé d'utiliser `pypy` pour optimiser le temps d'exécution :
```
pypy all_reversal_distances.py
```

----------


Distance DCJ
-------------------
Le script `dcj_distance.py` permet de calculer les distances DCJ exacte et estimée des deux génomes de drosophiles à 803 gènes. Pour cela, utilisez la commande 
```
python2 dcj_distance.py
```
Il est possible d'utiliser la fonction `dcj_distance` dans la console Python2 :
```
from dcj_distance import dcj_distance as dcj
p = [1, -5, 6, -8, 7, -4, -2, 3]
print dcj(p) 
```