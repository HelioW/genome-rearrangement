"""
generate all permutations of size 6, 7, 8
and compute the reversal distances
"""

from itertools import permutations
from reversal_distance_dfs import reversal_distance as rd


def all_six_reversal_distances():
    d = []
    for p in permutations(range(1, 7)):
        d.append(rd(list(p)))
    f = open('six_reversal_distances.txt', 'w')
    f.write(' '.join(map(str, d)))
    f.close()


def all_seven_reversal_distances():
    d = []
    for p in permutations(range(1, 8)):
        d.append(rd(list(p)))
    f = open('seven_reversal_distances.txt', 'w')
    f.write(' '.join(map(str, d)))
    f.close()


def all_eight_reversal_distances():
    d = []
    for p in permutations(range(1, 9)):
        d.append(rd(list(p)))
    f = open('eight_reversal_distances.txt', 'w')
    f.write(' '.join(map(str, d)))
    f.close()

# run using pypy
# all_six_reversal_distances()
# all_seven_reversal_distances()
# all_eight_reversal_distances()




