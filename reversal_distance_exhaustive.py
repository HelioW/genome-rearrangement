"""
compute the reversal distance between two unsigned permutations
using exhaustive iterative search, for n <= 7
"""

def apply_reversal(p, i, j):
    """apply a reversal to the permutation p between i and j-1"""
    p[i:j] = reversed(p[i:j])


def reversal_candidates_exhaustive(p):
    """
    generate all permutatations after one reversal from p
    """
    for i in xrange(len(p)-1):
        for j in xrange(i+2, len(p)+1):
            copy_p = p[:]
            apply_reversal(copy_p, i, j)
            yield copy_p
                
    
def reversal_distance_exhaustive(perm):
    """
    return the reversal distance of an unsigned permutatation
    using exhaustive search, for correctness testing
    """
    n = len(perm)
    sorted_perm = range(1, n+1)
    if perm == sorted_perm:
        return 0
    current_distance = 1
    current_perms = [perm]
    while True:
        next_perms = []
        for perm in current_perms:
            can_be_sorted = False
            for next_perm in reversal_candidates_exhaustive(perm):
                if next_perm == sorted_perm:
                    can_be_sorted = True
                    break
                next_perms.append(next_perm)
            if can_be_sorted:
                return current_distance
        current_perms = next_perms
        current_distance += 1
    return -1
