import sys
import os
import subprocess

def reversal_distance_haskell(a):
    n = len(a)
    try:
        return int(subprocess.check_output(['./reversal_distance', str(n),
                                       ' '.join(map(str, a))]))
    except subprocess.CalledProcessError, e:
        return 0
